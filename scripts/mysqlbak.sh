#!/bin/bash
#####################################################
#
# Filename:    mysqlbak.sh
# Author:      Noel Springer
# Email:       webmaster@gothisway.net
# Created:     2018-12-26
# Updated:     2020-01-05
#
# Description: Back up all databases (except ignored)
# as individual gzipped sql files.
# Usage:       ./path/to/mysqlbak.sh
#
# License:     MIT
# License URI: https://www.lnkto.com/mit-license/
# Copyright:   Noel Springer 2018-2020
#
#######################################################

# MySQL credentials
# Requires a ~/.my.cnf file with user, password and host specified.

### Commands ###
MYSQL="/usr/bin/mysql"
MYSQLDUMP="/usr/bin/mysqldump"
GPG="/usr/bin/gpg"
GZIP="/bin/gzip"
S3CMD="/usr/local/bin/s3cmd"

# Set HOME for cron
user="webspring"
HOME="/home/${user}"
# GPG passphrase file
gpgpw="$HOME/.gpg/gpg-topaz.pp"
gpgid="5AE23057C72B4562"
recipient="topaz.kaizen"
### Get date time for filename ###
now=$(date +"%Y-%m-%d-%H%M")

frequency[0]="daily"
frequency[1]="weekly"
frequency[2]="monthly"

### Delete files older than ###
days_old[0]=6
days_old[1]=21
days_old[2]=180

### Local backup directory ###
backup_dir_base="/data/bak/dbs/"
backup_dir[0]="${backup_dir_base}${frequency[0]}/"
backup_dir[1]="${backup_dir_base}${frequency[1]}/"
backup_dir[2]="${backup_dir_base}${frequency[2]}/"

### S3 related ###
s3bucket="s3://webspring"
client="topaz"

# Prefix should have a minumum value of "/" and should begin and end with "/".
s3prefix_base="/${client}/dbs/"
s3prefix[0]="${s3prefix_base}${frequency[0]}/"
s3prefix[1]="${s3prefix_base}${frequency[1]}/"
s3prefix[2]="${s3prefix_base}${frequency[2]}/"

### List of databases to exclude ###
ignore_db="
information_schema
mysql
performance_schema
phpmyadmin
test
"

### NOTHING TO EDIT BELOW HERE ###

### PROCESSES ###
### Check if backup directory exists and if not create it ###
for i in "${backup_dir[@]}"
  do
    if [[ ! -d $i ]]; then
      mkdir -p $i
    fi
  done;

### List databases ###
# dbs="$(${MYSQL} -u $mysql_user -p$mysql_pwd -h $mysql_host -Bse 'SHOW DATABASES')"
dbs="$(${MYSQL} -Bse 'SHOW DATABASES')"

### Loop through list of databases ###
for db in ${dbs}; do
  backup="yes"

  ## Check if any databases should be ignored ##
  if [[ -n $ignore_db ]]; then
    for i in $ignore_db
      do
        if [[ ${db} == $i ]]; then
          backup="no"
         # echo "$i database is being ignored!"
        fi
      done
  fi

  ## Backup databases not being ignored ##
  # Dump, compress and move to backup directory #
  if [[ ${backup} == yes ]]; then
    filename="$now-${db}.sql.gz"

    # Local backup
    ${MYSQLDUMP} --databases ${db} --max_allowed_packet=500M -f | ${GZIP} -9 > ${backup_dir[0]}/${filename}
    # If the day is 7 (Sunday) copy to weekly directory
    if [[ $(date +%u) -eq 7 ]]; then
       cp ${backup_dir[0]}/${filename} ${backup_dir[1]}/${filename}
    # If the date is the 1st of the month copy to the monthly directory
    elif [[ $(date +%-e) -eq 1 ]]; then
       cp ${backup_dir[0]}/${filename} ${backup_dir[2]}/${filename}
    fi

    # S3 compatible backup
    ${MYSQLDUMP} --databases ${db} --max_allowed_packet=500M -f | ${GZIP} -9 \
    | ${GPG} -e -r $recipient --batch --passphrase-file $gpgpw --cipher-algo AES256 - \
    | ${S3CMD} put - ${s3bucket}${s3prefix[0]}${filename}.gpg

    # If the day is 7 (Sunday) copy to weekly directory
    if [[ $(date +%u) -eq 7 ]]; then
       s3cmd cp ${s3bucket}${s3prefix[0]}${filename}.gpg ${s3bucket}${s3prefix[1]}${filename}.gpg
    # If the date is the 1st of the month copy to the monthly directory
    elif [[ $(date +%-e) -eq 1 ]]; then
       s3cmd cp ${s3bucket}${s3prefix[0]}${filename}.gpg ${s3bucket}${s3prefix[2]}${filename}.gpg
    fi
  fi
done

### Delete local files older than 'days_old' days ###
find ${backup_dir[0]} -mindepth 1 -mtime +${days_old[0]} -type f -delete
find ${backup_dir[1]} -mindepth 1 -mtime +${days_old[1]} -type f -delete
find ${backup_dir[2]} -mindepth 1 -mtime +${days_old[2]} -type f -delete

exit 0
