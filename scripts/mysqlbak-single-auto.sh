#!/bin/bash
###################################################

# Domain: website.example.com
# Server: server.example.com
#
# File:   mysqlbak-single-auto.sh
# Author: Noel Springer
# Email:  webmaster@gothisway.net
# 2019-10-21, 2020-09-07
# Description: Incorporates mysqldump, gzip, gpg
# and optional off-site backups to Backblaze B2
# with Restic.
# For automatation with crontab.
# For on-the-fly manual backups use mysqlbak-single.php
#
# Requirements:
#  1. ~/.my.cnf with [mysqldump] options for
#  user, password and host.
#  2. GPG key pair and passphrase file.
#  3. cli program for off-site backups
#     restic, rclone, s3cmd
#  4. Backblaze B2 account, bucket and folders, application key pair.
#  5. Restic installed and configured.
#
# Licence: MIT
# Copyright © 2020 Noel Springer
#
###################################################


#####################################################################
# Objectives
#
# 1. Backup database six (6) times per day to $HOME/bak/dbs/daily.
#    Set cron to (example):
#    15  00,04,08,12,16,20 * * /home/USERNAME/PATH/TO/mysqlbak-production.sh > /dev/null
# 2. Backup database once (1) per week to $HOME/bak/dbs/weekly.
# 3. Backup database once (1) per month to $HOME/bak/dbs/monthly.
# 4. Delete old backups on 7, 28, 180 day cycle for daily, weekly, monthly
#    directories respectively.
# 5. TO DO: Sync backups to off-site storage provider.
#
#####################################################################

# User
user='username'
HOME="/home/${user}"

# Database
database="${user}_example"

# Path to backup directory with trailing slash.
bak_path="${HOME}/bak/dbs/"
bak_path_daily="${bak_path}daily/"
bak_path_weekly="${bak_path}weekly/"
bak_path_monthly="${bak_path}monthly/"

## restic
# Path to restic config file
restic_conf=${HOME}/.restic/resticrc

## GPG
# Passphrase file.
ppfile="${HOME}/.gnupg/ppfile"
# Key-ID
# Use full numeric key-ID if using numeric value.
gpg_recipient='my.example.com'

# Number of days
delete_daily=7
delete_weekly=28
delete_monthly=180

# Set the hour to run the copy command
# for weekly and monthly backups.
# A cron job should include this hour.
# Two digit format from 00 to 23
copy_hour=00

## Do not edit below here ##

# Path to command executable
MYSQLDUMP=$(command -v mysqldump)
GPG=$(command -v gpg)
GZIP=$(command -v gzip)
RESTIC=$(command -v restic)

# Check for backup directories and create if absent
if [[ ! -d ${bak_path_daily} ]]; then
  mkdir ${bak_path_daily}
fi

if [[ ! -d ${bak_path_weekly} ]]; then
  mkdir ${bak_path_weekly}
fi

if [[ ! -d ${bak_path_monthly} ]]; then
  mkdir ${bak_path_monthly}
fi

# Some date stuff
hour=$(date '+%H') # hour (00..23))
dow=$(date '+%u')  # day of week (1..7); 1 is Monday)
dom=$(date '+%d')  # day of month (e.g., 01))
prefix=$(date '+%Y%m%d-%H%M')

# Command
set -euxo pipefail

filename="${prefix}-${database}.sql.gz.gpg"

${MYSQLDUMP} ${database} --max_allowed_packet=500M | ${GZIP} -9 | \
${GPG} --no-tty -e --cipher-algo AES256 --passphrase-file ${ppfile} \
-r ${gpg_recipient} > ${bak_path_daily}${filename}

## Copy backup file to weekly or monthly directory ##
# Weekly
if [[ ${dow} = 7 && ${hour} = ${copy_hour} ]]; then
  cp ${filename} ${bak_path_weekly}
fi

# Monthly
if [[ ${dom} = 01 && ${hour} = ${copy_hour} ]]; then
  cp ${filename} ${bak_path_monthly}
fi

# Delete old files
find ${bak_path_daily} -mindepth 1 -type f -mtime +$delete_daily -delete;
find ${bak_path_weekly} -mindepth 1 -type f -mtime +$delete_weekly -delete;
find ${bak_path_monthly} -mindepth 1 -type f -mtime +$delete_monthly -delete;

# Optional off-site backup.
# restic to Backblaze B2 (BB2)
source ${restic_conf}
${RESTIC} backup ${bak_path}
