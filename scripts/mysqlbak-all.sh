#!/bin/bash
#####################################################
#
# mysqlbak-all.sh
# Noel Springer 2018-12-26
# webmaster@gothisway.net
#
# Backup all databases to a single gzipped sql file
# Usage: /path/to/mysqlbak-all.sh
#
# License: MIT
# License URI: https://www.lnkto.com/mit-license/
# Copyright: Noel Springer 2018
#
#####################################################

# MySQL credentials
# Requires a ~/.my.cnf file with user, password and host specified

### Delete files older than ###
days_old=10

### Commands ###
MYSQL=/usr/bin/mysql
MYSQLDUMP=/usr/bin/mysqldump
GZIP=/bin/gzip
HOST=$(hostname)
user=$(whoami)

### Get date time for filename ###
now=$(date +"%Y%m%d-%H:%M")

### Local backup directory ###
backup_dir="/data/bak/dbs/all"

### NOTHING TO EDIT BELOW HERE ###

### PROCESSES ###
### Check if backup directory exists and if not create it ###
if [[ ! -d ${backup_dir} ]]; then
  mkdir -p ${backup_dir}
fi

  ## Backup databases
  # Dump, compress and move to backup directory
    filename="$now-${HOST}-${user}-all-db.sql.gz"
    ${MYSQLDUMP} --all-databases --max_allowed_packet=500M -f | ${GZIP} -9 > ${backup_dir}/${filename}

### Delete files older than 'days_old' days ###
# find ${backup_dir} -mindepth 1 -mtime +${days_old} -type f -delete

exit 0
