#!/bin/bash

### Local backup dump directory ###
backup_dir="/data/bak/dbs"

### Delete files older than ###
days_old=5

find ${backup_dir} -mindepth 1 -mtime +${days_old} -type f -delete
