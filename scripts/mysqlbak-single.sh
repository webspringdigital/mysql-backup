#!/bin/bash
##################################################################
#
# Filename: mysqlbak-single.sh
# Noel Springer 2018-12-26
# webmaster@gothisway.net
#
# Backup a single database as a gzipped sql file.
#
# Usage:
# /path/to/mysqlbak-single.sh database_name comment-no-spaces
#
# License: MIT
# License URI: https://www.lnkto.com/mit-license/
# Copyright: Noel Springer 2018
#
###################################################################

# MySQL credentials
# Requires a ~/.my.cnf file with user, password and host specified

### Delete files older than ###
days_old=10

### Commands ###
MYSQL="/usr/bin/mysql"
MYSQLDUMP="/usr/bin/mysqldump"
GZIP="/bin/gzip"

### Get date time for filename ###
now=$(date +"%Y-%m-%d-%H%M")

### Local backup directory ###
backup_dir="/data/bak/dbs/single"

### NOTHING TO EDIT BELOW HERE ###

### PROCESSES ###
## Check if backup directory exists and if not create it ##
if [[ ! -d ${backup_dir} ]]; then
  mkdir -p ${backup_dir}
fi

## Name of backup file ##
filename="$now-$1_$2.sql.gz"

## $1 is for database name. Test if database name is missing and if so exit ##
if [[ -z $1 ]]; then
  echo
  echo "Backup failed. Database name is missing for argument 1."
  echo
  exit 1
## $2 is for comment. Test if comment is missing and if so exit ##
  elif [[ -z $2 ]]; then
    echo
    echo "Backup failed. Comment is missing for argument 2. Please add a hyphenated comment."
    echo
    exit 1
fi
## Dump, compress and move to backup directory ##
${MYSQLDUMP} --databases $1 --max_allowed_packet=500M | ${GZIP} -9 > ${backup_dir}/${filename}

### Delete files older than 'days_old' days ###
find ${backup_dir} -mindepth 1 -mtime +${days_old} -type f -delete

exit 0
