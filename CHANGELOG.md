# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-09-11 - Updated release

Includes all changes from initial commit through to 2020-09-11.

* 2020-09-11 97b0c6b Remove path variable from filename declaration
* 2020-09-11 c393d2e Remove unnecessary MKDIR var
* 2020-09-11 b6968c3 Fix syntax for creating backup directories
* 2020-09-11 7369fc1 Change variable name for gpg recipient
* 2020-09-07 67c21b3 Change specific user info to generic
* 2020-09-07 f3cd3a9 Add single db backup for cron automation
* 2020-09-01 ce7e162 Ignore TODO.md
* 2020-09-01 74266e6 Increase days_old to 10
* 2020-01-06 61ebcea Move 'days_old' block
* 2020-01-06 09bc4ec Fix spelling and improve formatting
* 2020-01-06 ac466ee Update date in license
* 2020-01-05 61b7a27 (s3cmd-retention) Refactor and add weekly and monthly backups
* 2020-01-05 10a8859 Formatting changes for mysqlbak-single.sh
* 2020-01-03 3e79043 Add constants for gpg and s3cmd paths; Update date format
* 2020-01-03 44e9c2c Add encrypted off-site backups to S3; ignore phpmyadmin db
* 2020-01-03 0dc82b3 1. Change backup directory 2. Add '-f' flag for mysqldump
* 2020-01-03 790ea7f Change backup directory; minor formatting changes for STDOUT
* 2020-01-03 0a02048 1. Change backup directory 2. Add '-f' flag for mysqldump 3. Uncomment delete old files
* 2020-01-03 baebb31 Change backup directory
* 2019-01-07 cb69f2f Reduce days old to 5
* 2018-12-29 842a4d6 Update path to scripts in readme
* 2018-12-29 e12fb15 Minor formatting change to readme
* 2018-12-28 68e222d Initial commit
