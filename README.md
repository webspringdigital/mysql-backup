# MySQL Bak

## A collection of scripts for MySQL database backups

## Installation

* Clone or download this repository to a sub-directory of your home directory e.g. `${HOME}/bin/mysqlbak`.
* Create a `.my.cnf` file in the root of your home directory i.e. the user running the script `~/.my.cnf`
* Add the MySQL user, password and host

`.my.cnf`

```sql
[client]
user=mysql_user
password=mysql_password
host=localhost
```

The scripts will source the `.my.cnf` credentials in lieu of using the mysql user and password directly in the commands.

The settings in the `.my.cnf` file can be checked with `mysql --print-defaults`.

* Secure the file permissions
`chmod 400 ~/.my.cnf`

### Scripts

There are three backup scripts available in the "scripts" directory:

1. `mysqlbak.sh`
2. `mysqlbak-single.sh`
3. `mysqlbak-all.sh`

And one script for deleting old backup files:

1. `mysql-delete-old.sh`

Each file needs to be made executable:

```bash
cd /path/to/installation/directory
chmod +x scripts/*.sh
```

Near the top of each backup file (line 16/17) check your system for the paths for the `mysql`, `mysqldump` and `gzip` commands and adjust if necessary.

```bash
### Commands ###
MYSQL=/usr/bin/mysql
MYSQLDUMP=/usr/bin/mysqldump
GZIP=/bin/gzip

```
### 1. mysqlbak.sh

This script will backup all databases (except those that are specifically ignored) as individual files.
For each database an sql file will be created, the file gzipped and then moved to a backup directory.

#### Usage

**Example**

`~/bin/mysqlbak/scripts/mysqlbak.sh`

**Optional:** Create a cron job to run at your required frequency

e.g. use one of the following methods:

1. **Hourly:** Copy script to `/etc/cron.hourly`
2. **Daily:** Copy script to `/etc/cron.daily`
3. Add an entry in crontab

e.g. Backup all databases as individual gzipped files at 02:15 every two days.

```bash
crontab -e
15 2 */2 * * ${HOME}/bin/mysqlbak/scripts/mysqlbak.sh >/dev/null 2>&1

```

### 2. mysqlbak-single.sh

This script will backup a single database as a gzipped sql file and then move it to a backup directory.

#### Usage

This script requires two positional parameters

$1 = database_name
$2 = comment-with-hyphens

**Example:**

`~/bin/mysqlbak/scripts/mysqlbak-single.sh database_name add-post-cron-howto`

This works well with a shell alias e.g. add to `~/.bashrc` or `~/.bash_aliases` (or whatever alias file your shell uses).

e.g. `alias mybak='${HOME}/bin/mysqlbak-single.sh'`

then from the command line use the alias along with the database_name and the-comment.

`mybak database_name my-comment`

or set up an alias for a specific database:

e.g. `alias bakdb1='${HOME}/bin/mysqlbak/scripts/mysqlbak-single.sh database_db1'`

then only the comment is required on the command line.

`bakdb1 some-recent-change`

### 3. mysqlbak-all.sh

This script will create a backup of all databases as a single gzipped file and move it to the backup directory.

#### Usage

## Deleting old backups

The simplest way to delete older files is to use the script `delete-old.sh` set the number of days backups should be retained and create a daily cron job.

e.g. copy the script to `/etc/cron.daily`

Alternatively, each script has a command to delete old files near the bottom of the script. This line is commented out by default.
Set the number of days that backups should be retained (around line 18):

`days_old=7`

and uncomment the line:

`# find ${backup_dir} -mindepth 1 -mtime +${days_old} -type f -delete`

**NOTE:**

1. If using more than one script, uncomment this line in one script only.
2. If the script being used is run by cron less frequently than daily, then old files can only be deleted when the script runs.
